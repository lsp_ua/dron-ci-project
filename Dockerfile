FROM node:6.9.4

RUN apt-get update && apt-get install -y build-essential

ENV APP_HOME /usr/src/app

RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

ADD package.json $APP_HOME
RUN npm install -g node-gyp
RUN npm install -g pm2
RUN npm install

ADD . $APP_HOME


#RUN mkdir -p ~/.ssh/
#RUN cat ./ssh/id_rsa > ~/.ssh/id_rsa
#RUN cat ./ssh/id_rsa.pub > ~/.ssh/id_rsa.pub
#RUN cat ./ssh/known_hosts > ~/.ssh/known_hosts
#RUN chmod 600 ~/.ssh/id_rsa.pub
#RUN chmod 600 ~/.ssh/id_rsa
#RUN chmod 700 ~/.ssh

EXPOSE 3000
EXPOSE 9615
EXPOSE 23456
CMD ["pm2-docker", "process.json", "--web"]