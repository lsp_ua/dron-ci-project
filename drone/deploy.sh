#!/bin/bash
ls -la
echo "Updating staging Server"
echo "-----------------"
whereis docker
echo "-----------------"
docker pull localhost:5000/dron-ci-project:latest
docker stop dron-ci-project
docker rm dron-ci-project
docker rmi localhost:5000/dron-ci-project:current
docker tag localhost:5000/dron-ci-project:latest localhost:5000/dron-ci-project:current
docker run --name dron-ci-project -d -p 3017:3000 -v .:/usr/src/app -v /usr/src/app/node_modules localhost:5000/dron-ci-project:latest node app.js