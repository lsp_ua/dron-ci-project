#!/bin/bash
ls -la
echo "Updating staging Server"
echo "-----------------"
whereis docker
echo "-----------------"
docker pull bossi4ek/dron-ci-project
docker stop dron-ci-project
docker rm dron-ci-project
docker rmi bossi4ek/dron-ci-project:current
docker tag bossi4ek/dron-ci-project:latest bossi4ek/dron-ci-project:current
docker run --name dron-ci-project -d -p 3017:3000 bossi4ek/dron-ci-project:latest node app.js